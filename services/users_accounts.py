import logging

logger = logging.getLogger(__name__)


class UsersAccounts:
    # {"szymek" -> {"bochen": 12, "pozar": 45}} Szymek wisi bochenowi 12 i pozarowi 45 zl
    db = {}
    names = {}

    def __init__(self):
        self.insert_users()

    def insert_users(self):
        self.add_user("2607027426054154", "Szymon Seget", "35551905220000000000019315")
        self.add_user("3213669188703164", "Piotr Bochenek", "PL63249000050000400030900682")
        self.add_user("2692054267525697", "Ying Yuanxiao", "93551905220000000000037962")
        self.add_user("3581406398551540", "Sylwia Rosół", "PL64249010440000420042820498")


    def add_user(self, user_id, name, account_number):
        logger.info("ID: {}, name: {}, number: {}".format(user_id, name, account_number))
        self.db[user_id] = {}
        self.names[user_id] = {'name': name,
                               'account_number': account_number
                               }

    def exists_user(self, user_id):
        return user_id in self.db.keys()

    def delete_user(self, user_id):
        del self.db[user_id]

    def add(self, user_id, loaner, amount):
        if self.exists_user(user_id):
            if loaner in self.db[user_id].keys():
                self.db[user_id][loaner] = self.db[user_id][loaner] + amount
            else:
                self.db[user_id][loaner] = amount
        else:
            self.db[user_id] = {}
            self.db[user_id][loaner] = amount

    def get_loans_for(self, user_id):
        return self.db[user_id]

    def get_account(self, user_id):
        return self.names[user_id]['account_number']

    def get_name(self, user_id):
        return self.names[user_id]['name']
