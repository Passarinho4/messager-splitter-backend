#!/usr/bin/env python3
import logging
import os

import splitter

logger = logging.getLogger(__name__)

if __name__ == "__main__":

    app = splitter.create_app(config_name=os.getenv('FLASK_ENV', 'production'))

    if app.config.get('PREFERRED_URL_SCHEME') == 'https':
        logger.info("Starting HTTPS")
        app.run(host=app.config.get('HOST'), port=app.config.get('PORT'),
                ssl_context=(app.config.get('CERT'), app.config.get('KEY')))
    else:
        logger.info("Starting HTTP")
        app.run(host=app.config.get('HOST'), port=app.config.get('PORT'))
