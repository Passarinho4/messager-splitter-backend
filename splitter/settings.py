from services.users_accounts import UsersAccounts
from os.path import join

LOGGER_CONFIG = 'logging.ini'
CERTS_PATH = "splitter/certs"
USERS_ACCOUNTS_DB = UsersAccounts()


class BaseConfig(object):
    FB_ACCESS_TOKEN = "EAAF1LZBGkrzABAN5jMAQ3Ss1j9JkqjRkAMB0vKn5SQVJ7ylZCGjpQLHcOJVfoS6m298kLAqiXzGt8IW1T6LyDIgsyQUAbdrsloZC2eT8TwZBZAe9qnUkv5nRVWPPsdrcJz3fGHOJPxdNzCXucZBu9XJCVq354yQqZCVZAo1lZA2sEfpaD30725adP"
    DEBUG = False
    TESTING = False
    PREFERRED_URL_SCHEME = 'https'
    HOST = '0.0.0.0'
    PORT = "443"
    CERT = join(CERTS_PATH, 'fullchain.pem')
    KEY = join(CERTS_PATH, 'privkey.pem')


class DevelopmentConfig(BaseConfig):
    DEBUG = True
    TESTING = True
    PREFERRED_URL_SCHEME = 'http'
    PORT = "8443"


class TestingConfig(BaseConfig):
    DEBUG = False
    TESTING = True
    PREFERRED_URL_SCHEME = 'http'
    PORT = "8443"
