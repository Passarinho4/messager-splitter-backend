import logging

import requests
from flask import Blueprint, request, abort

from splitter.settings import USERS_ACCOUNTS_DB, BaseConfig

logger = logging.getLogger(__name__)

basic_api = Blueprint('basic_api', __name__)


@basic_api.route('/webhook', methods=["POST"])
def post_webhook():
    try:
        body = request.json
        logger.info("NO CIEKAWE")
        logger.info(body)
        obj = body['object']
        if obj == "page":
            for i in body['entry']:
                message = i['messaging'][0]
                sender = str(message['sender']['id'])
                message_content = str(message['message']['text'])
                logger.info("SENDER" + sender)
                logger.info("MESSAGE_CONTENT" + message_content)
                logger.info(USERS_ACCOUNTS_DB.get_loans_for(sender))
                results = USERS_ACCOUNTS_DB.get_loans_for(sender)
                for user in results.keys():
                    send_button_message_to_user(sender, user, results[user])
            return "EVENT_RECEIVED"
        else:
            abort(404)
    except Exception as e:
        logger.error("No jelo", e)
        return "EVENT_RECEIVED"


@basic_api.route('/test/<userId>', methods=["GET"])
def send_test(userId):
    results = USERS_ACCOUNTS_DB.get_loans_for(userId)
    for user in results.keys():
        send_button_message_to_user(userId, user, results[user])
    return "OK"


def send_text_message_to_user(user_id, message):
    logger.info("Sending message {} to user {}".format(message, USERS_ACCOUNTS_DB.get_name(user_id)))
    url = "https://graph.facebook.com/v2.6/me/messages"
    body = {
        "recipient": {"id": user_id},
        "message": {"text": message}
    }
    params = {"access_token": BaseConfig.FB_ACCESS_TOKEN}
    result = requests.post(url, json=body, params=params)
    logger.info(result.text)


def send_button_message_to_user(from_user_id, to_user_id, amount):
    logger.info("Sending Buttons to user {}".format(USERS_ACCOUNTS_DB.get_name(from_user_id)))

    endpoint = "https://publo.app/schedule_payment"
    params = "?fromAccount={}&toAccount={}&amount={}".format(from_user_id, to_user_id, amount)
    pis = endpoint + params + '&method=pis'
    alior = endpoint + params + '&method=alior'
    mastercard = endpoint + params + '&method=mastercard'

    url = "https://graph.facebook.com/v2.6/me/messages"
    body = {
        "recipient": {"id": from_user_id},
        "message": {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "button",
                    "text": format_message(to_user_id, amount),
                    "buttons": [
                        {
                            "type": "web_url",
                            "url": pis,
                            "title": "Zapłać przelewem"
                        },
                        {
                            "type": "web_url",
                            "url": alior,
                            "title": "Zapłać z Aliorem"
                        },
                        {
                            "type": "web_url",
                            "url": mastercard,
                            "title": "Zapłać z Mastercard"
                        }
                    ]
                }
            }
        }
    }
    params = {"access_token": BaseConfig.FB_ACCESS_TOKEN}
    result = requests.post(url, json=body, params=params)
    logger.info("Buttons sent with result: {}".format(result.text))


@basic_api.route('/webhook', methods=["GET"])
def get_webhook():
    verify_token = "RandomowyToken"

    mode = request.args.get("hub.mode")
    token = request.args.get("hub.verify_token")
    challange = request.args.get("hub.challenge")

    if mode == "subscribe" and token == verify_token:
        print("WEBHOOK_VERIFIED")
        return challange
    else:
        abort(403)


def format_message(user, amount):
    user_info = get_user_info_service(user)
    return "Jesteś dłużny " + user_info['first_name'] + " " + user_info['last_name'] + " kwotę " + str(amount) + "PLN."


@basic_api.route('/getUserInfo/<userId>', methods=["GET"])
def get_user_info(userId):
    return get_user_info_service(userId)


def get_user_info_service(user_id):
    url = "https://graph.facebook.com/v5.0/" + user_id
    params = {"access_token": BaseConfig.FB_ACCESS_TOKEN}
    result = requests.get(url, params=params).json()
    return result
