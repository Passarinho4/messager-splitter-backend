import logging
import requests

from flask import Blueprint, jsonify, request, redirect
from splitter.api.ms import send_button_message_to_user, send_text_message_to_user
from splitter.settings import USERS_ACCOUNTS_DB

basic_api = Blueprint('basic_api', __name__)
logger = logging.getLogger(__name__)


@basic_api.route('/users', methods=["POST", "GET"])
def add_user():
    if request.method == 'POST':
        user_id = str(request.json['userId'])
        USERS_ACCOUNTS_DB.add_user(user_id)
        logger.info("Creating user {}".format(user_id))
        return "OK"
    else:
        user_id = request.args.get('userId')
        result = USERS_ACCOUNTS_DB.get_loans_for(user_id)
        logger.info("Returning data for user {}".format(user_id))
        return jsonify(result)


@basic_api.route('/users/loans', methods=["POST"])
def add_loan():
    userId = request.json['userId']
    loaner = request.json['loaner']
    amount = float(request.json['amount'])
    USERS_ACCOUNTS_DB.add(userId, loaner, amount)
    return "OK"


@basic_api.route('/debts', methods=["POST"])
def add_debt():
    # {loanerId, debters:[{id, name, imageSrc, debts: [{id, description, price}]}]}
    loaner_id = str(request.json["loanerId"])
    USERS_ACCOUNTS_DB.insert_users()
    for debter in request.json["debters"]:
        for debt in debter['debts']:
            USERS_ACCOUNTS_DB.add(debter['id'], loaner_id, debt['price'])
        # Notify users with debts
        if loaner_id in USERS_ACCOUNTS_DB.get_loans_for(debter['id']).keys():
            logger.info(
                "Sending message to {} that he owns {} amount {}".format(USERS_ACCOUNTS_DB.get_name(debter['id']),
                                                                         USERS_ACCOUNTS_DB.get_name(loaner_id),
                                                                         debt['price']))
            debt = USERS_ACCOUNTS_DB.get_loans_for(debter['id'])[loaner_id]
            send_button_message_to_user(debter['id'], loaner_id, debt)
    return "OK"


@basic_api.route('/notify_user', methods=["GET"])
def notify_user():
    fromUser = request.args.get('fromUser')
    toUser = request.args.get('toUser')
    amount = request.args.get('amount')

    send_text_message_to_user(fromUser,
                              "Zlecono przelew dla {} na kwotę {}".format(USERS_ACCOUNTS_DB.get_name(toUser), amount))
    send_text_message_to_user(toUser, "Otrzymano przelew od {} na kwotę {}".format(USERS_ACCOUNTS_DB.get_name(fromUser),
                                                                                   amount))
    return """
<!DOCTYPE html>
<html>
<body onLoad="window.close();">
</body>
</html>
    """


@basic_api.route('/alior', methods=["GET"])
def alior_redrect():
    url = request.url.replace('https://publo.app/alior', 'http://104.40.209.110/aliorResult')
    logger.info("Handling Alior, request to: {}".format(url))
    response = requests.get(url)
    logger.info("Get response: {}".format(response.text))

    fromUser, toUser, amount = response.text.split(',')

    send_text_message_to_user(fromUser,
                              "Zlecono przelew dla {} na kwotę {}".format(USERS_ACCOUNTS_DB.get_name(toUser), amount))
    send_text_message_to_user(toUser, "Otrzymano przelew od {} na kwotę {}".format(USERS_ACCOUNTS_DB.get_name(fromUser),
                                                                                   amount))

    return """
<!DOCTYPE html>
<html>
<body onLoad="window.close();">
</body>
</html>
    """


@basic_api.route('/schedule_payment', methods=["GET"])
def schedule_payment():
    # http://104.40.209.110/test?fromAccount=35551905220000000000019315&fromUser=1234&toAccount=06551905220000000000023717&amount=1090.00&title=bomoge
    fromAccount = request.args.get('fromAccount')
    toAccount = request.args.get('toAccount')
    amount = float(request.args.get('amount'))
    method = request.args.get('method')

    logger.info(
        "Scheduling payment from {} to {} with amount {:.2f} via {}".format(
            USERS_ACCOUNTS_DB.get_name(fromAccount),
            USERS_ACCOUNTS_DB.get_name(toAccount),
            amount, method))

    from_account = USERS_ACCOUNTS_DB.get_account(fromAccount)
    to_account = USERS_ACCOUNTS_DB.get_account(toAccount)

    if method == 'pis':
        endpoint = "http://104.40.209.110/test"
    elif method == 'alior':
        endpoint = "http://104.40.209.110/alior"
    else:
        endpoint = "http://104.40.209.110/mastercard"

    params = "fromAccount={}&fromUser={}&toAccount={}&toUser={}&amount={:.2f}&title=Zwrot".format(from_account,
                                                                                                  fromAccount,
                                                                                                  to_account,
                                                                                                  toAccount,
                                                                                                  amount)

    url = endpoint + '?' + params
    logger.info("Sending to url: {}".format(url))
    response = requests.get(url)
    logger.info("Get redirect to: {}".format(response.text))
    return redirect(response.text, code=302)


@basic_api.route('/receipt', methods=["POST"])
def process_image():
    logger.info("Perform OCR for receipt")
    return """
{
	"message": "SUCCESS: Result available",
	"status": "done",
	"status_code": 3,
	"result": {
		"establishment": "NIP: 872-106-43-94",
		"validatedEstablishment": false,
		"date": "2019-11-17 16:18:00",
		"total": "307.500",
		"url": "",
		"phoneNumber": "",
		"paymentMethod": "",
		"address": "",
		"cash": "0.000",
		"change": "0.000",
		"validatedTotal": false,
		"subTotal": "0.000",
		"validatedSubTotal": false,
		"tax": "0.000",
		"taxes": [],
		"discount": "0.000",
		"rounding": "0.000",
		"discounts": [],
		"lineItems": [
			{
				"qty": 3,
				"desc": "PSTRĄG MACHOWSKI SOLO 3 szt.23.00",
				"unit": "",
				"price": "0.000",
				"symbols": [
					"B"
				],
				"discount": "0.000",
				"lineType": "",
				"descClean": "PSTRĄG MACHOWSKI SOLO szt.23.00 ZIEMNIAKI ZAPIEKANE szt - 5.50 16.500",
				"lineTotal": "69.000",
				"productCode": "",
				"customFields": []
			},
			{
				"qty": 1,
				"desc": "ZŁOCISTY DORSZ W PANIERCE 1 szt.+ 29.00",
				"unit": "",
				"price": "0.000",
				"symbols": [
					"B"
				],
				"discount": "0.000",
				"lineType": "",
				"descClean": "ZŁOCISTY DORSZ W PANIERCE szt.+ 29.00",
				"lineTotal": "29.000",
				"productCode": "",
				"customFields": []
			},
			{
				"qty": 1,
				"desc": "HERBATA Z CYTRYNA 1 szt.5.00",
				"unit": "",
				"price": "0.000",
				"symbols": [],
				"discount": "0.000",
				"lineType": "",
				"descClean": "HERBATA Z CYTRYNA szt.5.00",
				"lineTotal": "5.000",
				"productCode": "",
				"customFields": []
			},
			{
				"qty": 1,
				"desc": "TYMBARK 0,25 POMARAŃCZA 1 szt.- 5.00",
				"unit": "",
				"price": "0.000",
				"symbols": [
					"B"
				],
				"discount": "0.000",
				"lineType": "",
				"descClean": "TYMBARK 0,25 POMARAŃCZA szt.- 5.00 BARSZCZ Z KOŁDUNAMI szt.10.00",
				"lineTotal": "5.000",
				"productCode": "",
				"customFields": []
			},
			{
				"qty": 2,
				"desc": "ZUREK Z JAJKIEN I KIEŁBASA 2 szt. 12.00",
				"unit": "",
				"price": "0.000",
				"symbols": [
					"B"
				],
				"discount": "0.000",
				"lineType": "",
				"descClean": "ZUREK Z JAJKIEN I KIEŁBASA szt. 12.00",
				"lineTotal": "24.000",
				"productCode": "",
				"customFields": [],
				"supplementaryLineItems": {
					"above": [],
					"below": [
						{
							"qty": 0,
							"desc": "GRILLOWANE POLEDWICZKI W SOSIE KURKOWYM",
							"unit": "",
							"price": "0.000",
							"symbols": [],
							"discount": "0.000",
							"lineType": "",
							"descClean": "GRILLOWANE POLEDWICZKI W SOSIE KURKOWYM",
							"lineTotal": "0.000",
							"confidence": "0.500",
							"productCode": "",
							"customFields": []
						}
					]
				}
			},
			{
				"qty": 1,
				"desc": "1 szt.28.00",
				"unit": "",
				"price": "0.000",
				"symbols": [],
				"discount": "0.000",
				"lineType": "",
				"descClean": "szt.28.00",
				"lineTotal": "0.228",
				"productCode": "",
				"customFields": []
			},
			{
				"qty": 2,
				"desc": "KAPUSTA ZASMAZANA 200 gr 2 szt.7.50",
				"unit": "200 gr",
				"price": "0.000",
				"symbols": [
					"B"
				],
				"discount": "0.000",
				"lineType": "",
				"descClean": "KAPUSTA ZASMAZANA 200 gr szt.7.50",
				"lineTotal": "15.000",
				"productCode": "",
				"customFields": []
			},
			{
				"qty": 1,
				"desc": "TYMO SOK POMARAŃCZA IL 1 szt.12.00",
				"unit": "",
				"price": "0.000",
				"symbols": [
					"B"
				],
				"discount": "0.000",
				"lineType": "",
				"descClean": "TYMO SOK POMARAŃCZA IL szt.12.00",
				"lineTotal": "12.000",
				"productCode": "",
				"customFields": []
			},
			{
				"qty": 1,
				"desc": "FILET Z PSTRĄGA SOLO 1 szt.22.00",
				"unit": "",
				"price": "0.000",
				"symbols": [
					"B"
				],
				"discount": "0.000",
				"lineType": "",
				"descClean": "FILET Z PSTRĄGA SOLO szt.22.00",
				"lineTotal": "22.000",
				"productCode": "",
				"customFields": []
			},
			{
				"qty": 1,
				"desc": "FRYTKI Kuchnia 1 szt.- 6.00",
				"unit": "",
				"price": "0.000",
				"symbols": [
					"B"
				],
				"discount": "0.000",
				"lineType": "",
				"descClean": "FRYTKI Kuchnia szt.- 6.00 TYMBARK 0,25 POMARANCZA szt.- 5.00 00B",
				"lineTotal": "6.110",
				"productCode": "",
				"customFields": []
			},
			{
				"qty": 1,
				"desc": "KREM Z DYNI 1 szt. 9.00 9",
				"unit": "",
				"price": "0.000",
				"symbols": [
					"B"
				],
				"discount": "0.000",
				"lineType": "",
				"descClean": "KREM Z DYNI szt. 9.00 9",
				"lineTotal": "0.000",
				"productCode": "",
				"customFields": []
			},
			{
				"qty": 1,
				"desc": "FILET Z PSTRĄGA SOLO 1 szt.: 22.00",
				"unit": "",
				"price": "0.000",
				"symbols": [],
				"discount": "0.000",
				"lineType": "",
				"descClean": "FILET Z PSTRĄGA SOLO szt.: 22.00",
				"lineTotal": "22.000",
				"productCode": "",
				"customFields": []
			},
			{
				"qty": 1,
				"desc": "ZIEMNIAKI ZAPIEKANE 1 szt.- 5.50",
				"unit": "",
				"price": "0.000",
				"symbols": [],
				"discount": "0.000",
				"lineType": "",
				"descClean": "ZIEMNIAKI ZAPIEKANE szt.- 5.50 WARZYWA W POMIDORACH szt.7.00",
				"lineTotal": "5.500",
				"productCode": "",
				"customFields": []
			},
			{
				"qty": 1,
				"desc": "ZESTAW SURÓWEK 1 szt.7.50",
				"unit": "",
				"price": "0.000",
				"symbols": [
					"B"
				],
				"discount": "0.000",
				"lineType": "",
				"descClean": "ZESTAW SURÓWEK szt.7.50",
				"lineTotal": "7.500",
				"productCode": "",
				"customFields": []
			},
			{
				"qty": 0,
				"desc": "SP.OP.A : 5.00 PTU",
				"unit": "",
				"price": "0.000",
				"symbols": [],
				"discount": "0.000",
				"lineType": "",
				"descClean": "SP.OP.A : 5.00 PTU",
				"lineTotal": "0.930",
				"productCode": "",
				"customFields": []
			},
			{
				"qty": 0,
				"desc": "SP.OP.B : 302.50 PTU 8.00",
				"unit": "",
				"price": "0.000",
				"symbols": [],
				"discount": "0.000",
				"lineType": "",
				"descClean": "SP.OP.B : 302.50 PTU 8.00",
				"lineTotal": "22.110",
				"productCode": "",
				"customFields": []
			},
			{
				"qty": 0,
				"desc": "Suma PTU",
				"unit": "",
				"price": "0.000",
				"symbols": [],
				"discount": "0.000",
				"lineType": "",
				"descClean": "Suma PTU",
				"lineTotal": "23.340",
				"productCode": "",
				"customFields": []
			},
			{
				"qty": 0,
				"desc": "Suma : PLN",
				"unit": "",
				"price": "0.000",
				"symbols": [],
				"discount": "0.000",
				"lineType": "",
				"descClean": "Suma : PLN",
				"lineTotal": "307.500",
				"productCode": "",
				"customFields": []
			},
			{
				"qty": 3,
				"desc": "F066550 # 3",
				"unit": "",
				"price": "0.000",
				"symbols": [
					":"
				],
				"discount": "0.000",
				"lineType": "",
				"descClean": "F # 30A46CE634105838DB4E8559E6D49537",
				"lineTotal": "16.180",
				"productCode": "066550",
				"customFields": []
			}
		],
		"summaryItems": [
			{
				"qty": 0,
				"desc": "Karta",
				"unit": "",
				"price": "0.000",
				"symbols": [],
				"discount": "0.000",
				"lineType": "Total",
				"descClean": "Karta",
				"lineTotal": "307.500",
				"productCode": "",
				"customFields": []
			}
		],
		"subTotalConfidence": 0,
		"taxesConfidence": [],
		"discountConfidences": [],
		"totalConfidence": 0.3,
		"cashConfidence": 0,
		"changeConfidence": 0,
		"roundingConfidence": 0,
		"customFields": {
			"URL": "",
			"Country": "",
			"Currency": "",
			"VATNumber": "",
			"ExpenseType": "",
			"PaymentMethod": "",
			"CardLast4Digits": ""
		},
		"documentType": "receipt",
		"currency": "",
		"barcodes": [],
		"dateISO": "2019-11-17T16:18:00",
		"addressNorm": {
			"city": "",
			"state": "",
			"number": "",
			"street": "",
			"suburb": "",
			"country": "",
			"building": "",
			"postcode": ""
		},
		"expenseType": "None",
		"otherData": []
	},
	"success": true,
	"code": 202
}
    """
