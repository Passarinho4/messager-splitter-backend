#!/usr/bin/env python3
import logging.config


from flask import Flask
from flask_cors import CORS
from splitter import settings
from splitter.api.api import basic_api
from splitter.settings import USERS_ACCOUNTS_DB


logging.config.fileConfig(fname=settings.LOGGER_CONFIG
                          , disable_existing_loggers=False)

logger = logging.getLogger(__name__)

config = {
    "development": "splitter.settings.DevelopmentConfig",
    "testing": "splitter.settings.TestingConfig",
    "production": "splitter.settings.BaseConfig"
}


def create_app(config_name):

    logger.info("Configuration: " + config_name)
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    # Enable cors
    CORS(app)

    app.config.from_object(config[config_name])
    logger.info(app.config)

    app.register_blueprint(basic_api)
    return app
