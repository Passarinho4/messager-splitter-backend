from splitter import create_app, USERS_ACCOUNTS_DB


def app():
    return create_app(config_name='testing')


def test_app_start():
    assert app()


def test_get_user():
    flusk_app = app()
    res = flusk_app.test_client().get('/users/2607027426054154')
    assert res.status_code == 200
    # No loans by default
    assert b"{}" in res.data


def test_get_user():
    flusk_app = app()
    res = flusk_app.test_client().get('/test/ala')
    assert res.status_code == 404


def test_add_loan():
    flusk_app = app()
    res = flusk_app.test_client().post('/users/loans',
                                       json={'userId': '1',
                                             'loaner': '2',
                                             'amount': 10})

    res = flusk_app.test_client().post('/users/loans',
                                       json={'userId': '2',
                                             'loaner': '1',
                                             'amount': 20})

    res = flusk_app.test_client().post('/users/loans',
                                       json={'userId': '2',
                                             'loaner': '1',
                                             'amount': 30})

    assert res.status_code == 200
    assert b"OK" in res.data
    assert USERS_ACCOUNTS_DB.get_loans_for('1')['2'] == 10.0
    assert USERS_ACCOUNTS_DB.get_loans_for('2')['1'] == 50.0


def test_add_debt():
    flusk_app = app()
    res = flusk_app.test_client().post('/debts',
                                       json={'loanerId': '2607027426054154',
                                             'debters': [
                                                 {'id': '3213669188703164',
                                                  'debts': [
                                                      {'id': '2607027426054154', 'price': 20},
                                                  ]
                                                  }]})
    assert res.status_code == 200
    assert USERS_ACCOUNTS_DB.get_loans_for('3213669188703164')['2607027426054154'] == 20.0
