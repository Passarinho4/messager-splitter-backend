from services.users_accounts import UsersAccounts


def test_add_user():
    db = UsersAccounts()
    db.add_user(123, "Jan Kowalski", "1122334455")

    assert db.get_account(123) == "1122334455"
    assert db.get_name(123) == "Jan Kowalski"


def test_add_debt():
    db = UsersAccounts()

    # Register users
    db.add_user(1, "a", "123")
    db.add_user(2, "b", "456")

    db.add(1, 2, 10)
    db.add(1, 2, 10)

    db.add(2, 1, 10)

    assert db.get_loans_for(1)[2] == 20
    assert db.get_loans_for(2)[1] == 10
